var url = require('url');
var fs = require('fs');

function renderHTML(path, response){
  fs.readFile(path, function(error, data){
    if (error) {
      response.writeHead(404);
      response.write('file not found');
    }
    else {
      response.write(data);
    }
    response.end();
  });
}

module.exports = {
  handleRequest: function(request, response){
    response.writeHead(200, {'Content-Type' : 'text/html'});

    var path = url.parse(request.url).pathname;

    if (request.url.indexOf('.js') != -1){
      fs.readFile(__dirname + '/'+path, function (err, data) {
        if (err) console.log(err);
        response.writeHead(200, {'Content-Type': 'text/javascript'});
        response.write(data);
        response.end();
      });
    }
    if (request.url.indexOf('.css') != -1){
      fs.readFile(__dirname + '/'+path, function (err, data) {
        if (err) console.log(err);
        response.writeHead(200, {'Content-Type': 'text/css'});
        response.write(data);
        response.end();
      });
    }
    if (request.url.indexOf('.html') != -1 ){
      fs.readFile(__dirname + '/'+path, function (err, data) {
        if (err) console.log(err);
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(data);
        response.end();
      });
    }
    if (path == '/subpage1' ){
      fs.readFile(__dirname + '/'+path+'.html', function (err, data) {
        if (err) console.log(err);
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(data);
        response.end();
      });
    }
  }
}
