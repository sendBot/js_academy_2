
var mongoose = require('mongoose');

//gentes schema

var employeesSchema = mongoose.Schema({
  first_name:{
    type:String,
    require:true
  },
  last_name:{
    type:String,
    require:true
  },
  hobby:{
    type:String
  },
  age:{
    type:String
  },
  create_date:{
    type:Date,
    default:Date.now
  }
});
var Employees = module.exports = mongoose.model('Employees', employeesSchema);

//get all employees
module.exports.getEmployees = function(callback, limit){
  Employees.find(callback).limit(limit);
}
//get one employees
module.exports.getEmployeeById = function(id, callback){
  Employees.findById(id, callback);
}

//add employees
module.exports.addEmployee = function(employee, callback){
  Employees.create(employee, callback);
}

//update employees
module.exports.updateEmployee = function(id, employee, options, callback){
  var query = {_id: id};
  var update = {
    fist_name: employee.first_name,
    last_name: employee.last_name,
    hobby: employee.hobby,
    age: employee.age,
  }
  Employees.findOneAndUpdate(query, update, options, callback);
}

//remove employee
module.exports.removeEmployee = function(id, callback){
  var query = {_id: id};
  Employees.remove(query, callback)
}
