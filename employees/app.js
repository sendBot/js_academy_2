var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");

//app.use(express.static(__dirname+'/client'));

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(allowCrossDomain);

Employees = require('./models/employees');

//connect to mongose
url = 'mongodb://localhost/employees'
mongoose.connect('mongodb://localhost/employees');
var db = mongoose.connection;


app.get('/', function(req, res){
  res.send('use /api/employees');
});

app.get('/api/employees', function(req, res){
  Employees.getEmployees(function(err, employees){
    if(err){
      throw err;
    }
    res.json(employees);
  });
});

app.get('/api/employees/:_id', function(req, res){
    Employees.getEmployeeById(req.params._id, function(err, employee){
      if(err){
        throw err;
      }
      res.json(employee);
    });
});

app.post('/insert', function(req, res){
  var employee = req.body;
  var item = {
    first_name: employee.first_name,
    last_name:employee.last_name,
    hobby:employee.hobby,
    age:employee.age
  };
  mongoose.connect(url, function(err, db){
    assert.equal(null, err);
    db.collection('employees').insertOne(item, function(){
      assert.equal(null, err);
      db.close();
    });
  });
    Employees.addEmployee(employee, function(err, employee){
      if(err){
        throw err;
      }
      res.json(employee);
    });
});

app.put('/api/employees/:_id', function(req, res){
  var id = req.params._id;
  var employee = req.body;
    Employees.updateEmployee(id, employee, {}, function(err, employee){
      if(err){
        throw err;
      }
      res.json(employee);
    });
});

app.delete('/api/employees/:_id', function(req, res){
    var id = req.params._id;
    Employees.removeEmployee(id, function(err, employee){
      if(err){
        throw err;
      }
      res.json(employee);
    });
});

app.listen(4000);
console.log('running on port 4000');
