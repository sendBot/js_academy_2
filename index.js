/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}
function openClose(){
  if(document.getElementById("main").style.marginLeft === '0px'){
    openNav();
  }
  else{
    closeNav();
  }
}


function setActiveSubmenu(evt) {
    // Declare all variables
    var i, tabcontent, buttonLink;

    // Get all elements with class="tablinks" and remove the class "active"
    buttonLinks = document.getElementsByClassName("buttonLink");
    for (i = 0; i < buttonLinks.length; i++) {
        buttonLinks[i].className = buttonLinks[i].className.replace(" clicked", "");
    }
    // Show the current tab, and add an "active" class to the button that opened the tab
    evt.currentTarget.className += " clicked";
}
