$(function() {
  FetchAll();
});

var data = '';
var id_map = {};
var tableHTML = "<caption>employees</caption>";
var all_headers = [];
var xhttp = new XMLHttpRequest();



var getData = function() {
  return new Promise(function(resolve, reject){
  xhttp.onreadystatechange = function(data_json) {
    if (this.readyState == 4 && this.status == 200) {
      resolve(xhttp.response);
    }
  };
  xhttp.open("GET", 'http://localhost:4000/api/employees', false);
  xhttp.send(null);
});
};



var addData = function() {
  xhttp.onreadystatechange = function(data_json) {
    if (this.readyState == 4 && this.status == 200) {
      console.log("Success");
      FetchAll();
    }
  };
  xhttp.open("POST", 'http://localhost:4000/insert', false);
  xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xhttp.send(JSON.stringify(getAndFormData()));
};



var Remove = function(index) {
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log("element removed from DB:");
      FetchAll();
    }
  };
  xhttp.open("DELETE", 'http://localhost:4000/api/employees/' +id_map[index] , false);
  xhttp.send();
};


var Count = function(){
  var amount = data.length;
  var display_count = document.getElementById("records_amount");
  if (amount) {
    display_count.innerHTML = amount + ' records';
  } else {
    display_count.innerHTML = 'No records';
  }
};//Count


var FetchAll = function() {
  var promiseGetData = getData();
    promiseGetData.then(function(data_json){
      data = data_json = JSON.parse(data_json);
      genetareTableBody(data_json);
      Count();
    }).catch(function(error){
      console.log(error);
    });
};//FetchAll


var getAndFormData = function() {
  var name = document.getElementById("firstname");
  var lastname = document.getElementById("lastname");
  var hobby =  document.getElementById("hobby_form");
  var age =  document.getElementById("age_form");
  var result = {
    "first_name": name.value,
    "last_name" : lastname.value,
    "hobby" : hobby.value,
    "age" : age.value
  };
  name.value='';
  lastname.value='';
  hobby.value='';
  age.value='';
  return JSON.parse(JSON.stringify(result));
};


var generateHeaders = function(data){
  var headers = [];
  var headersText='<tr>';
  for (var i = 0 ; i < data.length ; i++){
    for (var key in data[i]) {
      if(key === '__v'){continue;}
      if(!headers.includes(key)){
        all_headers.push(key);
        headers.push(key);
        headersText+="<th id="+key+" onclick='Sort("+headers.indexOf(key)+")'>"+key+"<span class='caret'></span></th>";
      }
    }
  }
  headersText +="</tr>";
  return [tableHTML + headersText, headers] ;
};//this.generateHeaders



var genetareTableBody = function(data) {
  var resultTable = generateHeaders(data);
  var tesultText ='';
  for (var i = 0 ; i <  data.length ; i++){
    id_map[i] = data[i]._id ;
    tesultText += '<tr>';
    for (var k in resultTable[1]){
      var tempHead = resultTable[1][k];
      var tempContent = data[i][tempHead];
      if (tempContent){
        tesultText +='<td>'+data[i][tempHead]+'</td>';
      }
      else {
        data[i][tempHead] = '';
        tesultText += '<td></td>';
      }
    }
    tesultText +='<td style="width:50px;"><button onclick="Edit(' + i + ')" class="btn btn-primary">edit</button></td>';
    tesultText +='<td style="width:60px;"><button onclick="Remove('+ i +')" class="btn btn-danger">remove</button></td>';
    tesultText += '</tr>';
  }
  document.getElementById("mainTable").innerHTML = resultTable[0] + tesultText;
};//genetareTableBody



var Edit = function(index) {
  document.getElementById('editInput').style.display = "block";//make edit inpyt visible
  var editName = document.getElementById('edit_name');
  var editLastName = document.getElementById('edit_lastname');
  var editHobby = document.getElementById('edit_hobby');
  var editAge = document.getElementById('edit_age');
  editName.value = data[index].first_name; //displat value from array in input field
  editLastName.value = data[index].last_name;
  editHobby.value = data[index].hobby;
  editAge.value = data[index].age;
  //var self = this; //self is being used to maintain a reference to the original this even as the context is changing.

  document.getElementById('saveEdit').onsubmit = function() {
    var data_to_send = JSON.stringify({
        "first_name": editName.value,
        "last_name" : editLastName.value,
        "hobby" : editHobby.value,
        "age" : editAge.value
      });

    if (editName.value){ //if element inserty is not a null

      var sendEditRequest = function() {
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            console.log("element edited in DB");
            FetchAll();
          }
        };
        xhttp.open("PUT", 'http://localhost:4000/api/employees/' +id_map[index] , false);
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.send(data_to_send);
      };
      sendEditRequest();
      document.getElementById('editInput').style.display = "none";
    }
  };//onSubmit()
};//Edit()


var Sort = function(index){
  var parameter = all_headers[index];
  if (document.getElementsByClassName(parameter).length === 0 ){
    data.sort(function(a, b){
      return a[parameter].localeCompare(b[parameter]);
    });
    genetareTableBody(data);
    document.getElementById(parameter).className += parameter;
  }//sort descending

  else {
    data.sort(function(a, b){
      return b[parameter].localeCompare(a[parameter]);
    });
    genetareTableBody(data);
    document.getElementById(parameter).className ='';
  }//sort ascending
};//this.Sort


function CloseInput(){
  document.getElementById('editInput').style.display = "none";
}
