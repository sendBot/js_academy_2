$( document ).ready(function() {
    photoGallery();

    $("#carouselExampleIndicators").on('slid.bs.carousel', function () {
      setTitle();
    });

});

var addresses = {"DSC_0267" : "355/31920927756_02081814a0_c","DSC_0263" : "307/31809896822_c61f2a43a6_c" ,"DSC_0186" : "520/31809755552_e27f3a3a2b_c" ,"DSC_0703" : "567/31117787484_52e95c5da3_c" ,"DSC_0510" : "395/31584325220_7925f3716d_h", "DSC_0512" : "742/31148483023_a57af6334b_c", "DSC_0515" : "377/31920764826_4dd21d7de0_z", "DSC_0649" : "451/31148539853_38e010076f_c", "DSC_0497" : "522/31810005322_4776461154_z"};

function photoGallery (){

    var galleryText ='';
    var indicator = 0;
    var bootstrapGallery ='';
    var indicatorText ='';

  for (name in addresses){
    var http = "http://c1.staticflickr.com/1/"+addresses[name]+".jpg";
    galleryText +="<div><div onmouseover='displayPreiew("+'"'+name+'"'+")' onmouseleave='clearPreview()' onclick='setMainPhoto("+indicator+")' style='float: left;'><h3>"+name+".jpg</h3></div>";
    galleryText +="<div id="+'"'+name+'"'+" style='display: none;  width: 200px; height: 200px; position:absolute; margin-left:150px;'><img style='height:inherit;' src="+http+"></div></div>";
    galleryText += "<div class='clearfix'></div>";
    if (indicator === 0 ){
      indicatorText += '<li data-target="#carouselExampleIndicators" data-slide-to="'+indicator+'" class="active"></li>';
      bootstrapGallery += '<div class="item active" onclick="show_full_screen('+"'"+name+"'"+')"><img src="http://c1.staticflickr.com/1/'+addresses[name]+'.jpg" style="height: 500px; width:auto;" name="'+name+'"></div>';
    }
    else{
      indicatorText += '<li data-target="#carouselExampleIndicators" data-slide-to="'+indicator+'"></li>';
      bootstrapGallery += '<div class="item" onclick="show_full_screen('+"'"+name+"'"+')"><img src="http://c1.staticflickr.com/1/'+addresses[name]+'.jpg" style="height: 500px; width: auto;" name="'+name+'"></div>';
    }
    indicator++;
  }

  document.getElementById("gallery").innerHTML = galleryText;
  document.getElementById("indicators").innerHTML = indicatorText;
  document.getElementById("carousel-innerr").innerHTML = bootstrapGallery;
  setTitle();
};

function displayPreiew(name) {
  var elem = document.getElementById(name);
  elem.style.display = "block";
}

function clearPreview(){
  for (title in addresses){
     document.getElementById(title).style.display = "none";
  }
}

function setMainPhoto(index) {
  $('.carousel').carousel(index);
  $('.carousel').carousel('pause');
  setTitle();
}

function setTitle() {
  var elem = document.getElementsByClassName("active");
  document.getElementById("main-title").innerHTML = elem[2].children["0"].name;
}

function show_full_screen(name){
  document.getElementById('single-preview').style.display = "block";
  var http = "http://c1.staticflickr.com/1/"+addresses[name]+".jpg";
  document.getElementById('single-preview').innerHTML = "<img src=" +"'"+http+"'"+" style='max-height:100%; height:100%'></img>";

}
function close_full_screen(){
  document.getElementById('single-preview').style.display = "none";
}
